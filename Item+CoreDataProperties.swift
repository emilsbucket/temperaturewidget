//
//  Item+CoreDataProperties.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-04-14.
//
//

import Foundation
import CoreData


extension Item {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Item> {
        return NSFetchRequest<Item>(entityName: "Item")
    }

    @NSManaged public var timestamp: Date?

}

extension Item : Identifiable {

}
