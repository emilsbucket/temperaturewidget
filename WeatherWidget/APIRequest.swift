//
//  APIRequest.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-04-14.
//

import Foundation
import Combine

enum APIError: Error {
    case error
    case parseError
}

struct APIRequest<T: Decodable> {
    private func decode(_ data: Data) -> AnyPublisher<T, APIError> {
        let decoder = JSONDecoder()
        
        return Just(data)
            .decode(type: T.self, decoder: decoder)
            .mapError({_ in APIError.parseError})
            .eraseToAnyPublisher()
    }
    
    func performRequest(to url: URL) -> AnyPublisher<T,APIError> {
        let urlRequest = URLRequest(url: url)
        
        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .mapError({ _ in APIError.error})
            .flatMap { value in
                decode(value.data)
            }
            .eraseToAnyPublisher()
    }
}
