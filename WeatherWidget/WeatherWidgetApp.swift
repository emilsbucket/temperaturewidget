//
//  WeatherWidgetApp.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-04-14.
//

import SwiftUI

@main
struct WeatherWidgetApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            let weatherService = WeatherService(coreDataWorker: CoreDataWorker(persistenceController: persistenceController))
            ContentView(viewModel: WeatherViewModel(weatherService: weatherService))
        }
    }
}
