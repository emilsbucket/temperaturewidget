//
//  CoreLocationWorker.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-04-29.
//

import Foundation
import CoreLocation
import Combine

enum LocationError: Error {
    case unauthorized
}

protocol CoreLocationWorking {
    func authorizeLocationServices() -> AnyPublisher<Void, LocationError>
    func retreiveUserLocation() -> AnyPublisher<CLLocation, LocationError>
}

class CoreLocationWorker: NSObject, CoreLocationWorking {
    private let locationManager: CLLocationManager
    private var authorizeSubject = PassthroughSubject<Void, LocationError>()
    private var locationSubject = PassthroughSubject<CLLocation, LocationError>()
    private let previousAuthStatus: CLAuthorizationStatus
    
    init(locationManager: CLLocationManager) {
        self.locationManager = locationManager
        previousAuthStatus = locationManager.authorizationStatus
        super.init()
    }
    
    func authorizeLocationServices() -> AnyPublisher<Void, LocationError> {
        switch locationManager.authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            return Just(Void())
                .setFailureType(to: LocationError.self)
                .eraseToAnyPublisher()
        case .notDetermined:
            self.locationManager.delegate = self
            authorizeSubject = PassthroughSubject<Void, LocationError>()
            locationManager.requestAlwaysAuthorization()
            return authorizeSubject
                .eraseToAnyPublisher()
        default:
            return Fail(error: LocationError.unauthorized)
                .eraseToAnyPublisher()
        }
    }
    
    func retreiveUserLocation() -> AnyPublisher<CLLocation, LocationError> {
        self.locationManager.delegate = self
        locationSubject = PassthroughSubject<CLLocation, LocationError>()
        locationManager.requestLocation()
        return locationSubject
            .eraseToAnyPublisher()
    }
}

extension CoreLocationWorker: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            if !previousAuthStatus.isAuthorised {
                authorizeSubject.send()
                authorizeSubject.send(completion: .finished)
            }
        case .notDetermined:
            break
        default:
            authorizeSubject.send(completion: .failure(.unauthorized))
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let firstLocation = locations.first else { return }
        locationSubject.send(firstLocation)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("CL ERROR")
    }
}

private extension CLAuthorizationStatus {
    var isAuthorised: Bool {
        switch self {
        case .authorizedAlways, .authorizedWhenInUse:
            return true
        default:
            return false
        }
    }
}
