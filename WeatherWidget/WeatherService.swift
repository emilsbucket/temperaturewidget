//
//  WeatherService.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-04-26.
//

import Foundation
import Combine
import SwiftUI
import CoreLocation

protocol WeatherServicing {
    func fetchWeather(at location: CLLocation) -> AnyPublisher<WeatherData, APIError>
}

class WeatherService: WeatherServicing {
    private let coreDataWorker: CoreDataWorking
    init(coreDataWorker: CoreDataWorking) {
        self.coreDataWorker = coreDataWorker
    }
    
    func fetchWeather(at location: CLLocation) -> AnyPublisher<WeatherData, APIError> {
        return coreDataWorker.fetchWeatherDataAt(longitude: location.coordinate.longitude,
                                    latitude: location.coordinate.latitude)
            .catch({ _ in self.fetchWeatherFromNetwork(at: location)
                .handleEvents(
                    receiveOutput: { weatherData in
                        self.coreDataWorker.storeWeatherDataFor(
                            longitude: location.coordinate.longitude,
                            latitude: location.coordinate.latitude,
                            weatherData: weatherData
                        )
                    }
                )
            })
            .eraseToAnyPublisher()
    }
    
    private func fetchWeatherFromNetwork(at location: CLLocation) -> AnyPublisher<WeatherData, APIError> {
        let weatherUrlString = "https://api.openweathermap.org/data/2.5/weather?units=metric&lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&appid=4f2228b22dde89a5d6e6cf2009b496c1"
        let temperatureUrlString = "http://api.temperatur.nu/tnu_1.17.php?lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&graph&cli=wallin_test_app"
        guard let currentWeatherUrl = URL(string: weatherUrlString),
              let temperatureAPIUrl = URL(string: temperatureUrlString) else {
            return Fail(outputType: WeatherData.self, failure: APIError.error)
                .eraseToAnyPublisher()
        }
        
        let currentWeatherRequest = APIRequest<CurrentWeatherDataDTO>()
        let currentWeather = currentWeatherRequest.performRequest(to: currentWeatherUrl)
        
        let temperatureRequest = APIRequest<TemperatureAPIDTO>()
        let temperature = temperatureRequest.performRequest(to: temperatureAPIUrl)
        
        return Publishers.Zip(currentWeather, temperature)
            .map({ WeatherData(currentWeatherDataDTO: $0.0, temperatureAPIDTO: $0.1) })
            .eraseToAnyPublisher()
    }
}

enum ViewModelError: Error {
    case locationError(LocationError)
    case apiError(APIError)
    
    init(locationError: LocationError) {
        self = .locationError(locationError)
    }
    
    init(apiError: APIError) {
        self = .apiError(apiError)
    }
}
