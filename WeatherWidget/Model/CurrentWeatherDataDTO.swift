//
//  CurrentWeatherDataDTO.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-04-27.
//

import Foundation

struct CurrentWeatherDataDTO: Decodable {
    let name: String
    let wind: WindDTO
    let main: Main
    
    struct Main: Decodable {
        let temp: Double
    }
}

struct WindDTO: Decodable {
    let speed: Double
    let deg: Int
}
