//
//  TemperatureAPIDTO.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-04-28.
//

import Foundation


struct TemperatureAPIDTO: Decodable {
    
    let stations: [Station]
    
    struct Station: Decodable {
        let title: String
        let temperature: Double
        let distance: Double
    }
}

extension TemperatureAPIDTO.Station {
    enum CodingKeys: String, CodingKey {
        case title, temp, dist
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        let temperatureString = try container.decode(String.self, forKey: .temp)
        let distanceString = try container.decode(String.self, forKey: .dist)
        guard let temperatureValue = Double(temperatureString),
              let distanceValue = Double(distanceString) else {
            throw APIError.parseError
        }
        self.temperature = temperatureValue
        self.distance = distanceValue
    }
}


