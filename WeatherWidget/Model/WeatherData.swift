//
//  WeatherData.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-04-28.
//

import Foundation

struct WeatherData {
    let temperature: Temperature
    let windDirection: Int
    let windSpeed: Double
    
    struct Temperature {
        let value: Double
        let source: Source
        
        enum Source {
            case temperatureAPI(distance: Double, stationName: String)
            case weatherAPI(name: String)
        }
    }
}

extension WeatherData {
    init(currentWeatherDataDTO: CurrentWeatherDataDTO, temperatureAPIDTO: TemperatureAPIDTO) {
        if let station = temperatureAPIDTO.stations.first {
            self.temperature = WeatherData.Temperature(value: station.temperature,
                                                       source: WeatherData.Temperature.Source.temperatureAPI(distance: station.distance, stationName: station.title))
        } else {
            self.temperature = WeatherData.Temperature(value: currentWeatherDataDTO.main.temp,
                                                       source: WeatherData.Temperature.Source.weatherAPI(name: currentWeatherDataDTO.name))
        }
        
        self.windDirection = currentWeatherDataDTO.wind.deg
        self.windSpeed = currentWeatherDataDTO.wind.speed
    }
}

extension WeatherData.Temperature.Source: CustomStringConvertible {
    var description: String {
        switch self {
        case .temperatureAPI(distance: let distance, stationName: let name):
            return "Source: Temperatur.nu, distance: \(distance), station: \(name)"
        case .weatherAPI(name: let name):
            return "Source: Open weather map, station: \(name)"
        }
    }
}

extension WeatherData: CustomStringConvertible {
    var description: String {
        return "Temperature: \(temperature.value), source: \(temperature.source)\nwind speed: \(windSpeed), wind direction: \(windDirection)"
    }
}
