//
//  CoreDataWorker.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-04-29.
//

import Foundation
import Combine

enum CoreDataWorkerError: Error {
    case empty
}

protocol CoreDataWorking {
    func fetchWeatherDataAt(longitude: Double, latitude: Double) -> AnyPublisher<WeatherData,CoreDataWorkerError>
    func storeWeatherDataFor(longitude: Double, latitude: Double, weatherData: WeatherData)
}

class CoreDataWorker: CoreDataWorking {
    
    private let persistenceController: PersistenceController
    init(persistenceController: PersistenceController) {
        self.persistenceController = persistenceController
    }
    
    func fetchWeatherDataAt(longitude: Double, latitude: Double) -> AnyPublisher<WeatherData, CoreDataWorkerError> {
        return Just(WeatherData(temperature: .init(value: -256.0, source: .weatherAPI(name: "Nacka")), windDirection: 2, windSpeed: 5.0))
            .setFailureType(to: CoreDataWorkerError.self)
            .eraseToAnyPublisher()
    }
    
    func storeWeatherDataFor(longitude: Double, latitude: Double, weatherData: WeatherData) {
        
    }
    
    private func fetchAndUpdateWeatherDataAt(longitude: Double, latitude: Double) {
        
    }
}
