//
//  ContentView.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-04-14.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @ObservedObject var viewModel: WeatherViewModel
    
    var body: some View {
        ZStack {
            WeatherBackgroundView()
            VStack {
                GeometryReader { geometry in
                    ZStack {
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle())
                            .foregroundColor(Color.white)
                            .isHidden(!viewModel.loading)
                        Text(viewModel.temperature)
                            .font(.system(size: 88))
                            .multilineTextAlignment(.center)
                            .foregroundColor(.white)
                    }
                    .frame(width: geometry.size.width,height: geometry.size.height * 0.3, alignment: .center)
                }
                Button("Fetch weather", action: { viewModel.fetchWeather() })
                    .padding(EdgeInsets(top: 5.0, leading: 5.0, bottom: 5.0, trailing: 5.0))
                    .foregroundColor(Color.white)
            }
        }
    }
}

struct WeatherBackgroundView: View {
    var body: some View {
        Color.black
            .ignoresSafeArea()
    }
}

extension View {
    @ViewBuilder func isHidden(_ isHidden: Bool) -> some View {
        if isHidden {
            hidden()
        } else {
            self
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        return ContentView(viewModel: WeatherViewModel(weatherService: WeatherService(coreDataWorker: CoreDataWorker(persistenceController: PersistenceController.shared))))
    }
}
