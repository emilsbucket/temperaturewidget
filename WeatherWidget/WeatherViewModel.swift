//
//  WeatherViewModel.swift
//  WeatherWidget
//
//  Created by Emil Lundgren on 2021-05-03.
//

import Foundation
import CoreLocation
import SwiftUI
import Combine

class WeatherViewModel: ObservableObject {
    @Published var temperature: String = ""
    @Published var loading: Bool = false
    
    private let weatherService: WeatherServicing
    
    init(weatherService: WeatherServicing) {
        self.weatherService = weatherService
    }
    
    private var subscriptions = Set<AnyCancellable>()
    private var locationWorker = CoreLocationWorker(locationManager: CLLocationManager())
    
    func fetchWeather() {
        loading = true
        locationWorker.authorizeLocationServices()
            .flatMap({ self.locationWorker.retreiveUserLocation() })
            .mapError({ ViewModelError(locationError: $0) })
            .flatMap({ location in
                self.weatherService.fetchWeather(at: location)
                    .mapError({ ViewModelError(apiError: $0)})
            })
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { [weak self] result in
                guard let self = self else {
                    return
                }
                switch result {
                case .failure(let error):
                    print("ERROR: \(error)")
                    self.updateStateFor(result: Result.failure(error))
                case .finished:
                    print("Finished")
                }
            }
                  , receiveValue: { [weak self] data in
                guard let self = self else {
                    return
                }
                print(data.description)
                self.updateStateFor(result: Result.success(data))
            })
            .store(in: &subscriptions)
    }
    
    private func updateStateFor(result: Result<WeatherData, ViewModelError> ) {
        self.loading = false
        switch result {
        case .success(let weatherData):
            self.temperature = "\(weatherData.temperature.value)°C"
        case .failure:
            self.temperature = "N/A"
        }
    }
}
